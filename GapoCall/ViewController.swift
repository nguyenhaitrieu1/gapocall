//
//  ViewController.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/1/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import UIKit
import GapoMQTT
import JanusGateway
import WebRTC

class ViewController: UIViewController {
    
    @IBOutlet weak var txtCallee: UITextField!
    @IBOutlet private weak var localVideoView: UIView?
    @IBOutlet private weak var remoteVideoView: UIView!
    @IBOutlet weak var usernameLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    private var authAPI: AuthAPIService?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let inputData = AppConfig.getCacheDictionary(key: "input_data") as? [String : String] {
            usernameLabel.text = inputData["phone"]
            passwordLabel.text = inputData["pass"]
        }
        
        if let inputData = AppConfig.getCacheDictionary(key: "cache_outgoing") as? [String : String] {
            txtCallee.text = inputData["user_id"]
        }

        authAPI = AuthAPIService()
        
    }
    
    func showCallingVC() {
    }
    
    @IBAction func callAction() {
        self.view.endEditing(true)
        guard let callee = txtCallee.text else {
            return
        }
        let inputData = ["user_id": callee]
        AppConfig.setCacheDictionary(value: inputData, key: "cache_outgoing")

        UserInfoManager.shared.outgoingTargetId = callee
        
        let vc = CallingVC()
        vc.modalPresentationStyle = .fullScreen
        vc.callService = CallService(outgoing: callee, type: .video)
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func answerAction() {
        self.view.endEditing(true)
    }

    @IBAction func loginAction() {
        self.view.endEditing(true)
        guard let phone = self.usernameLabel.text, let password = passwordLabel.text else { return }
        authAPI?.login(phone: phone, password: password) {[weak self]  (response, error) in
            guard let response = response else { return }
            UserInfoManager.shared.user = response.data
            UserInfoManager.shared.user?.saveConfig(key: "cache_user_info")
            
            let inputData = ["phone": phone, "pass": password]
            AppConfig.setCacheDictionary(value: inputData, key: "input_data")

            print("login_data: \(response)")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.connectMQTT()
        }
    }
}
