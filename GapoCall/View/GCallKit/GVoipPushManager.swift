//
//  GVoipPushManager.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import UIKit
import PushKit
import CallKit

class GVoipPushManager: NSObject, PKPushRegistryDelegate {
    static let shared = GVoipPushManager()
    
    // Register for VoIP notifications
    func voipRegistration() {
        
        // Create a push registry object
        let mainQueue = DispatchQueue.main
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    //MARK: - PKPushRegistryDelegate
    // Handle updated push credentials
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        print(credentials.token)
        let deviceToken = credentials.token.map { String(format: "%02x", $0) }.joined()
        print("pushRegistry -> deviceToken :\(deviceToken)")
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        print("pushRegistry:didInvalidatePushTokenForType:")
    }
    
    // Handle incoming pushes
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        print(payload.dictionaryPayload)
        GCallManager.shared.handleIncomingPushEvent(payload: payload, completion: { (status, error) in
            print("incoming call: \(String(describing: error))")
        })
    }
}
