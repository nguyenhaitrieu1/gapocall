//
//  GCallManager.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import PushKit
import CallKit
import AVFoundation

class GCallManager: NSObject, CXProviderDelegate {
    
    //MARK: - Init
    static let shared = GCallManager()
    
    override init() {
        super.init()
        provider.setDelegate(self, queue: nil)
    }
    var call: GCallModel?
    
    lazy var provider: CXProvider = {
        let configuration = CXProviderConfiguration(localizedName: appName)
        configuration.supportsVideo = true
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportedHandleTypes = [.generic, .phoneNumber]
        return CXProvider(configuration: configuration)
    }()
    
    lazy var callController: CXCallController = {
        return CXCallController()
    }()
    
    var showingCallKitUUID: UUID?
    var watingForUpdateCallKit: (() -> Void)?
    
    func configureAudioSession() {
        print("CONFIGURE AUDIO SESSION")
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(.playAndRecord, mode: .videoChat)
            try audioSession.setPreferredSampleRate(44100.0)
            try audioSession.setPreferredIOBufferDuration(0.005)
        } catch  {
            print("Cấu hình audio session cho callkit thất bại")
        }
    }
}
