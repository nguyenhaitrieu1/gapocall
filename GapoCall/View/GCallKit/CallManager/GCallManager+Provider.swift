//
//  GCallManager+Provider.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import CallKit
import AVFoundation

let appName = "Gapo"

extension GCallManager {
    func reportIncomingCall(phone: String, callerName: String, isVideo: Bool, completion: @escaping (Bool, UUID, Error?) -> ()) {
        let callUpdate = CXCallUpdate()
        callUpdate.hasVideo = isVideo
        callUpdate.remoteHandle = CXHandle(type: .generic, value: phone)
        callUpdate.localizedCallerName = callerName
        let uuid = UUID()
        
        showingCallKitUUID = uuid
        provider.reportNewIncomingCall(with: uuid, update: callUpdate) {[weak self] (error) in
            guard let self = self else { completion(false, uuid, error); return }
            
            self.watingForUpdateCallKit?()
            self.watingForUpdateCallKit = nil
            self.showingCallKitUUID = nil
            
            if error == nil {
                self.configureAudioSession()
                completion(true, uuid, nil)
            } else {
                completion(false, uuid, error)
            }
        }
        
    }
    
    func reportUpdatedCall(phone: String, callerName: String, isVideo: Bool, uuid: UUID) {
        let callUpdate = CXCallUpdate()
        callUpdate.hasVideo = isVideo
        callUpdate.remoteHandle = CXHandle(type: .generic, value: phone)
        callUpdate.localizedCallerName = callerName
        provider.reportCall(with: uuid, updated: callUpdate)
    }
    
    func startCall(phone: String, calleeName: String, isVideo: Bool, janusCall: GJanusCall) {
        if (call != nil) {
            return
        }
        
        let handle = CXHandle(type: .generic, value: phone)
        call = GCallModel(isIncoming: false)
        call?.uuid = UUID()
        call?.janusCall = janusCall
        call?.callId = janusCall.callId
        
        let startCallAction = CXStartCallAction(call: (call?.uuid)!, handle: handle)
        startCallAction.isVideo = isVideo
        startCallAction.contactIdentifier = calleeName
        
        let transaction = CXTransaction()
        transaction.addAction(startCallAction)
        requestTransaction(transaction: transaction)
    }
    
    func endCall() {
        if let uuid = self.call?.uuid {
            provider.updateConfiguration(includesCallsInRecents: true)
            let endCallAction = CXEndCallAction(call: uuid)
            let transaction = CXTransaction()
            transaction.addAction(endCallAction)
            requestTransaction(transaction: transaction)
        }
    }
    
    func holdCall(hold: Bool) {
        if let uuid = self.call?.uuid {
            let holdCallAction = CXSetHeldCallAction(call: uuid, onHold: hold)
            let transaction = CXTransaction()
            transaction.addAction(holdCallAction)
            requestTransaction(transaction: transaction)
        }
    }
    
    func requestTransaction(transaction: CXTransaction) {
        callController.request(transaction) { [unowned self] (error) in
            if error != nil {
                print("requestTransaction: \(String(describing: error?.localizedDescription))")
                // End Callkit va xoa current call
                self.endCall()
                self.call = nil
                
                // Co man hinh calling => dismiss
            }
        }
    }
}

// MARK: - Callkit Delegate
@available(iOS 10.0, *)
extension GCallManager {
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("CXStartCallAction")
        configureAudioSession()
        
        provider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        provider.reportOutgoingCall(with: action.callUUID, connectedAt: nil)
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("======== CALLKIT ANSWERED \(action.callUUID)")
        if call?.uuid?.uuidString != action.callUUID.uuidString {
            action.fulfill()
            return
        }
        
        call?.answered = true
        call?.answerAction = action
        call?.clean()
        answerCallWithCondition()
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("======== CALLKIT ENDED \(action.callUUID)")
        if let uuidString = call?.uuid?.uuidString, uuidString != action.callUUID.uuidString {
            action.fulfill()
            return
        }
        
        call?.clean()
        call = nil
        guard let call = call, let janusCall = call.janusCall else {
            action.fulfill()
            return
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        print("CXSetHeldCallAction")
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        print("CXSetMutedCallAction")
        mute { (status) in
            if status {
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("didActivate audioSession")
        call?.audioIsActived = true
        answerCallWithCondition()
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        print("didDeactivate audioSession")
        call?.audioIsActived = false
    }
    
    func providerDidReset(_ provider: CXProvider) {
        print("providerDidReset")
    }
    
    func providerDidBegin(_ provider: CXProvider) {
        print("providerDidBegin")
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetGroupCallAction) {
        print("CXSetGroupCallAction")
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXPlayDTMFCallAction) {
        print("CXPlayDTMFCallAction")
        
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print("timedOutPerforming")
        
    }
    
    private func providerConfig(includesCallsInRecents: Bool) -> CXProviderConfiguration {
        let configuration = CXProviderConfiguration(localizedName: appName)
        configuration.supportsVideo = true
        configuration.maximumCallsPerCallGroup = 1
        configuration.maximumCallGroups = 1
        configuration.supportedHandleTypes = [.generic]
        if #available(iOS 11.0, *) {
            configuration.includesCallsInRecents = includesCallsInRecents
        }
        return configuration
    }
}
