//
//  GCallManager+Actions.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

// MARK: - Call Actions

extension GCallManager {
    
    func answer(shouldChangeUI: Bool = true) {
        if let callingVC = InstanceManager.shared.callingVC, shouldChangeUI {
            //Update UI
        }
        
        guard let stringeeCall = call?.janusCall else { return }
        
        if let answerAction = self.call?.answerAction {
            answerAction.fulfill()
            self.call?.answerAction = nil
            return
        }
        //Todo: Gọi tới hàm answer của janus
    }
    
    func reject(_ janusCall: GJanusCall? = nil) {
        call?.rejected = true
        
        var callNeedToReject: GJanusCall? = nil
        if let seCall = janusCall {
            callNeedToReject = seCall
        } else if let seCall = call?.janusCall {
            callNeedToReject = seCall
        } else {
            return
        }
        
        //Todo: gọi tới hàm từ chối cuộc gọi của janus
    }
    
    func hangup(_ janusCall: GJanusCall? = nil) {
        var callNeedToHangup: GJanusCall? = nil
        if let seCall = janusCall {
            callNeedToHangup = seCall
        } else if let seCall = call?.janusCall {
            callNeedToHangup = seCall
        } else {
            return
        }
        
        //Todo: Gọi tới hàm hangup của janus
    }
    
    func mute(completion: ((Bool) -> Void)? = nil) {
        guard let callingVC = InstanceManager.shared.callingVC, let janusCall = call?.janusCall else {
            completion?(false)
            return
        }
        //Todo: Gọi tới hàm mute của Janus + Update trạng thái trên UI
        completion?(true)
    }
    
    func answerCallWithCondition(shouldChangeUI: Bool = true) {
        guard let callkitCall = call else {
            let _ = CallingVC()
            return }
        
        if callkitCall.isIncoming && callkitCall.answered && (callkitCall.audioIsActived || callkitCall.answerAction != nil) {
            answer(shouldChangeUI: shouldChangeUI)
        }
    }
}
