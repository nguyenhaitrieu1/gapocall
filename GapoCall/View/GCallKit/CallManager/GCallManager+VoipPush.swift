//
//  GCallManager+VoipPush.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import PushKit

extension GCallManager {
    func handleIncomingPushEvent(payload: PKPushPayload, completion: @escaping (Bool, Error?) -> ()) {
        //FAKE Data
        let alias = "Lê Đình Mạnh"
        let from = "0353096867"
        
        reportIncomingCall(phone: from, callerName: alias, isVideo: true) { (status, uuid, error) in
            DispatchQueue.main.async {
                completion(status, error)
                if (status) {
                    // Thành công thì gán lại uuid
                    self.call?.uuid = uuid
                } else {
                    self.call?.clean()
                    self.call = nil
                }
            }
        }
    }
}
