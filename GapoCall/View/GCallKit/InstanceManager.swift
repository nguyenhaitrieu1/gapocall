//
//  InstanceManager.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit

class InstanceManager {
    static let shared = InstanceManager()
    
    var viewController: UIViewController?
    var callingVC: UIViewController?
}
