//
//  GCallModel.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import CallKit

class GCallModel {
    var serial = 0
    var callId: String?
    var janusCall: GJanusCall?
    var uuid: UUID?
    
    var answered = false
    var rejected = false
    
    var audioIsActived = false
    var isIncoming = false
    var answerAction: CXAnswerCallAction?
    
    var timer: Timer?
    var counter = 0
    
    init(isIncoming: Bool) {
        self.isIncoming = isIncoming
        if isIncoming {
            startTimer()
        }
    }
    
    private func startTimer() {
        if timer != nil { return }
        
        stopTimer()
        timer = Timer(timeInterval: 2, target: self, selector: #selector(GCallModel.handleCallTimeOut), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .default)
        timer?.fire()
    }
    
    @objc private func handleCallTimeOut() {
        counter += 2
        
        if counter >= 28 {
            stopTimer()
            if !answered && !rejected {
                GCallManager.shared.endCall()
            }
        }
    }
    
    private func stopTimer() {
        CFRunLoopStop(CFRunLoopGetCurrent())
        timer?.invalidate()
        timer = nil
    }
    
    func clean() {
        stopTimer()
    }
    
    deinit {
        stopTimer()
    }
}
