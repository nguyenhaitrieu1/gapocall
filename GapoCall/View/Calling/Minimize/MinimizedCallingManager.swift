//
//  MinimizedCallingManager.swift
//  GapoCallKit
//
//  Created by manh.le on 10/2/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit

enum MinimizedType: Int, CaseIterable {
    case audio
    case video
    
    var tag: Int {
        switch self {
        case .video:
            return 1002
        default:
            return 1001
        }
    }
}

class MinimizedCallingManager: NSObject {
    
    //MARK: - Minimized Audio
    
    static func showMinimizedAudio() {
        if let keywindow = UIApplication.shared.keyWindow, keywindow.viewWithTag(MinimizedType.audio.tag) == nil {
            let view: UIView = MimimizedAudioView.fromNib()
            view.frame = CGRect(x: 0, y: 0, width: Int(keywindow.frame.width), height: 0)
            view.tag = MinimizedType.audio.tag
            keywindow.addSubview(view)
            
            var topPadding = 0
            if #available(iOS 11.0, *) {
                topPadding = Int(keywindow.safeAreaInsets.top)
            }

            UIView.animate(withDuration: 0.5, delay: 0.3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                view.frame = CGRect(x: 0, y: topPadding, width: Int(keywindow.frame.width), height: 32)
            }, completion: { (finish) in
                
            })
        }
    }
    
    static func showMinimizedVideo() {
        if let keywindow = UIApplication.shared.keyWindow, keywindow.viewWithTag(MinimizedType.video.tag) == nil {
            var topPadding: CGFloat = 0
            if #available(iOS 11.0, *) {
                topPadding = keywindow.safeAreaInsets.top
            }

            let videoWidth = CGFloat(112)
            let videoHeight = CGFloat(244)
            let videoRightPadding = CGFloat(16)
            let videoTopPadding = CGFloat(67)

            let originX = keywindow.frame.width - videoWidth - videoRightPadding
            let originY = topPadding + videoTopPadding

            let view: UIView = MinimizedVideoView.fromNib()
            view.frame = CGRect(x: Int(originX), y: Int(originY), width: Int(keywindow.frame.width), height: 0)
            view.tag = MinimizedType.video.tag
            keywindow.addSubview(view)
            
            UIView.animate(withDuration: 0.5, delay: 0.3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                view.frame = CGRect(x: Int(originX), y: Int(originY), width: Int(keywindow.frame.width), height: Int(videoHeight))
            }, completion: { (finish) in
                
            })
        }
    }

    static func hideMinimizedCalling() {
        if let keywindow = UIApplication.shared.keyWindow {
            let minimizedVideo = keywindow.viewWithTag(MinimizedType.video.tag)
            minimizedVideo?.removeFromSuperview()
            
            let minimizedAudio = keywindow.viewWithTag(MinimizedType.audio.tag)
            minimizedAudio?.removeFromSuperview()

        }
    }
}
