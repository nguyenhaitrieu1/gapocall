//
//  MinimizedVideoView.swift
//  GapoCallKit
//
//  Created by manh.le on 10/2/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class MinimizedVideoView: UIView {
    @IBOutlet private var previewView: UIView!
    
    var videoDataOutput: AVCaptureVideoDataOutput!
    var videoDataOutputQueue: DispatchQueue!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()

    override func awakeFromNib() {
        super.awakeFromNib()
        previewView.layer.masksToBounds = true
        previewView.layer.cornerRadius = 6
        setupAVCapture(position: .front)
    }
}

extension MinimizedVideoView: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    //Get the device (Front or Back)
    func getDevice(_ position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        if #available(iOS 10.0, *) {
            return AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: position)
        } else {
            let devices: NSArray = AVCaptureDevice.devices() as NSArray;
            for de in devices {
                let deviceConverted = de as! AVCaptureDevice
                if(deviceConverted.position == position){
                    return deviceConverted
                }
            }
        }
        return nil
    }
    
    func setupAVCapture(position: AVCaptureDevice.Position) {
        session.sessionPreset = AVCaptureSession.Preset.vga640x480
        configCaptureDevice(position: position)
        beginSession()
    }
    
    func configCaptureDevice(position: AVCaptureDevice.Position) {
        guard let device = getDevice(position) else {
            return
        }
        captureDevice = device
    }
    
    func configDeviceInput() {
        var deviceInput: AVCaptureDeviceInput!
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            guard deviceInput != nil else {
                print("error: cant get deviceInput")
                return
            }
            if self.session.canAddInput(deviceInput){
                self.session.addInput(deviceInput)
            }
        } catch let error as NSError {
            deviceInput = nil
            print("error: \(error.localizedDescription)")
        }
    }
    
    func configDeviceOutput() {
        videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.alwaysDiscardsLateVideoFrames=true
        videoDataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue")
        videoDataOutput.setSampleBufferDelegate(self, queue:self.videoDataOutputQueue)
        
        if session.canAddOutput(self.videoDataOutput){
            session.addOutput(self.videoDataOutput)
        }
        
        videoDataOutput.connection(with: .video)?.isEnabled = true
        session.startRunning()
    }
    
    func configPreviewLayer() {
        previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let rootLayer :CALayer = self.previewView.layer
        rootLayer.masksToBounds=true
        previewLayer.frame = rootLayer.bounds
        rootLayer.addSublayer(self.previewLayer)
    }
    
    func beginSession(){
        configDeviceInput()
        configDeviceOutput()
        configPreviewLayer()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // do stuff here
    }
    
    // clean up AVCapture
    func stopCamera(){
        session.stopRunning()
    }
}

