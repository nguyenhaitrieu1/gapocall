//
//  CallingVC+Model.swift
//  GapoCallKit
//
//  Created by manh.le on 10/2/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation

enum CallingType: Int, CaseIterable {
    case audio
    case video
}

enum PlayerState: Int, CaseIterable {
    case hidden
    case minimized
    case fullScreen
}

enum CallScreenType {
    case outgoing
    case incoming
    case calling
}

enum SignalingState {
    case calling
    case ringring
    case answered
    case busy
    case ended
}

enum CallMediaState {
    case connected
    case disconnected
}

struct CallControl {
    var isIncoming = false
    var isVideo = false
    
    var from = ""
    var to = ""
    var username = ""
    var displayName: String {
        if username.count > 0 {
            return username
        } else {
            return isIncoming ? from : to
        }
    }
    
    var isMute = false
    var isSpeaker = false
    var localVideoEnabled = true
    var signalingState: SignalingState = .calling
    var mediaState: CallMediaState = .disconnected
    
    var callScreenType: CallScreenType {
        if (signalingState == .answered) {
            return .calling
        } else {
            return isIncoming ? .incoming : .outgoing
        }
    }
}

