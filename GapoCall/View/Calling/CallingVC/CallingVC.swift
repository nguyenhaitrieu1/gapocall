//
//  CallingVC.swift
//  GapoCallKit
//
//  Created by manh.le on 10/1/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import JanusGateway
import WebRTC

class CallingVC: UIViewController {
    @IBOutlet private weak var headerView: UIView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var stateCallingLabel: UILabel!
    @IBOutlet private weak var stateView: UIView!
    @IBOutlet private weak var avatarView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet public weak var  miniView: UIView!
    @IBOutlet public weak var  remoteVideoView: UIView!
    @IBOutlet public weak var  previewView: UIView!
    @IBOutlet private weak var bottomView: UIView!
    
    @IBOutlet private weak var outsideVideoView: UIView!

    lazy var callingAudioControl: CallingControlView = {
        return setupControl(type: .audio, state: .calling)
    }()
    
    lazy var answeredAudioControl: CallingControlView = {
        return setupControl(type: .audio, state: .answered)
    }()
    
    lazy var callingVideoControl: CallingControlView = {
        return setupControl(type: .video, state: .calling)
    }()
    
    lazy var answeredVideoControl: CallingControlView = {
        return setupControl(type: .video, state: .answered)
    }()
        
    var callingType: CallingType = .video
    var callService: CallService? {
        didSet {
            callService?.callClient?.videoViewDelegate = self
        }
    }
    
    //Camera Capture requiered properties
    var videoDataOutput: AVCaptureVideoDataOutput!
    var videoDataOutputQueue: DispatchQueue!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()
    var usingFrontCamera = true
    var playerState: PlayerState = .fullScreen
    
    var stateVC: PlayerState = .fullScreen

    var callControll: CallControl = CallControl()
    
    let minimizedFrame: CGRect = {
        var topPadding = 20
        if #available(iOS 11.0, *) {
            topPadding = Int(UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0)
        }
        let x = UIScreen.main.bounds.width - 112 - 16
        let y = CGFloat(topPadding + 48 + 12)
        let coordinate = CGPoint(x: x, y: y)
        return CGRect(x: x, y: y, width: 112, height: 200)
    }()
    
    deinit {
        let className = NSStringFromClass(self.classForCoder)
        print("~> \(className) did release...")
        callService = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        MinimizedCallingManager.hideMinimizedCalling()
        setupUI()
    }
    
    func setupUI() {
        self.view.backgroundColor = .clear
        self.view.addBlurEffect()
        self.addTapAction()
        self.addOutsideView()
        
        callingAudioControl.alpha = 1
        callingAudioControl.isUserInteractionEnabled = true
        
        avatarView.layer.masksToBounds = true
        avatarView.layer.cornerRadius = avatarView.frame.width/2
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width/2
        
        switch callingType {
        case .video:
            hiddenView(true)
        default:
            hiddenView(false)
        }
    }
    
    func addOutsideView() {
        self.view.addSubview(outsideVideoView)
        outsideVideoView.autoPinEdgesToSuperviewEdges()
        outsideVideoView.alpha = 0
    }
    
    func hiddenView(_ isHidden: Bool) {
        nameLabel.isHidden = isHidden
        stateCallingLabel.isHidden = isHidden
        avatarView.isHidden = isHidden
        avatarImageView.isHidden = isHidden
    }
    
    //MARK: - Calling Control
    func setupControl(type: CallingType, state: SignalingState) -> CallingControlView {
        let controlView = CallingControlView(type: type, state: state)
        controlView.alpha = 0
        controlView.isUserInteractionEnabled = false
        bottomView.addSubview(controlView)
        controlView.autoPinEdgesToSuperviewEdges()
        controlView.completion = {[weak self] (type, action) in
            self?.handleCallingControl(type, action)
        }
        return controlView
    }
    
    func handleCallingControl(_ type: CallingType, _ action: CallingActionType) {
        switch action {
        case .accept:
            self.callService?.createAnswer()
            if type == .video {
                UIView.animate(withDuration: 0.5, animations: {
                    self.callingVideoControl.alpha = 0
                    self.answeredVideoControl.alpha = 1
                }, completion: { (finish) in
                    self.callingVideoControl.isUserInteractionEnabled = false
                    self.answeredVideoControl.isUserInteractionEnabled = true
                })
                
            } else {
                UIView.animate(withDuration: 0.5, animations: {
                    self.callingAudioControl.alpha = 0
                    self.answeredAudioControl.alpha = 1
                }, completion: { (finish) in
                    self.callingAudioControl.isUserInteractionEnabled = false
                    self.answeredAudioControl.isUserInteractionEnabled = true
                })
            }
            self.acceptAction()
        case .camera:
            self.switchCameraAction()
        case .decline:
            self.hangupAction()
        case .hangup:
            self.hangupAction()
        default:
            break
        }
    }
    
    func switchCameraAction() {
        usingFrontCamera = !usingFrontCamera
    }
    
    func acceptAction() {
        movePreviewVideo()
    }
    
    func hangupAction() {
        callService?.hangup(completion: { (error) in
            
        })
        self.dismiss(animated: true, completion: nil)
//        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
//            self.bottomView.alpha = 0
//        }, completion: {[weak self] (finish) in
//            self?.hideView()
//        })
    }
    
    func updateButtonAction() {
        //        switch callControll.callScreenType {
        //        case .calling:
        //            
        //        case .outgoing:
        //            
        //        default:
        //            
        //        }
    }
    
    func movePreviewVideo() {
        switch playerState {
        case .fullScreen:
            let trasform = CGAffineTransform.init(scaleX: minimizedFrame.size.width/UIScreen.main.bounds.width, y: minimizedFrame.size.height/UIScreen.main.bounds.height)
            
            UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseInOut, animations: {
                self.previewView.transform = trasform
                self.previewView.center = self.minimizedFrame.center
            }, completion: {[weak self] (finish) in
                self?.playerState = .minimized
                self?.previewView.layer.masksToBounds = true
                self?.previewView.layer.cornerRadius = 12
            })
        default:
            
            let trasform = CGAffineTransform.identity
            UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseInOut, animations: {
                self.previewView.transform = trasform
                self.previewView.center = self.view.center
            }, completion: {[weak self] (finish) in
                self?.playerState = .fullScreen
                self?.previewView.layer.masksToBounds = true
                self?.previewView.layer.cornerRadius = 0
            })
        }
    }
    
    func activeFullScreenView(active: Bool) {
        self.previewView.alpha  = active ? 1 : 0
        self.headerView.alpha   = active ? 1 : 0
        self.stateView.alpha    = active ? 1 : 0
        self.bottomView.alpha   = active ? 1 : 0

        self.outsideVideoView.alpha = active ? 0 : 1
    }
    
    //MARK: - Actions
    
    @IBAction func minimizedAction(_ sender: UIButton) {
        self.stateVC = .minimized
        self.animate()
//        MinimizedCallingManager.showMinimizedAudio()
    }
    
    func hideView() {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 5, options: [.beginFromCurrentState], animations: {
            self.view.frame.origin = hiddenOrigin
        })
    }
}

//MARK: - Client Delegate

extension CallingVC: GClientVideoViewDelegate {
    func rtcClient(client : GCallClient, didReceiveLocalVideoView localVideoView: RTCVideoRenderer) {
        DispatchQueue.main.async {
            if let localVideo = localVideoView as? UIView {
                self.previewView?.addSubview(localVideo)
                localVideo.autoPinEdgesToSuperviewEdges()
            }
        }
    }
    
    func rtcClient(client : GCallClient, didReceiveRemoteVideoView remoteVideoView: RTCEAGLVideoView) {
        self.remoteVideoView.addSubview(remoteVideoView)
        remoteVideoView.autoPinEdgesToSuperviewEdges()
    }
}
