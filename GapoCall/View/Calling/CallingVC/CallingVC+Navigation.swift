//
//  CallingVC+Navigation.swift
//  GapoCallKit
//
//  Created by manh.le on 10/5/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit

let outsideVideoSize = CGSize(width: 122, height: 244)
let outsideVideoOrigin: CGPoint = {
    var topPadding = 20
    if #available(iOS 11.0, *) {
        topPadding = Int(UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0)
    }
    
    let x = UIScreen.main.bounds.width - 122 - 16
    let y = CGFloat(topPadding + 48)
    let coordinate = CGPoint(x: x, y: y)
    return coordinate
}()

let hiddenOrigin: CGPoint = {
    let y = UIScreen.main.bounds.height
    let x = CGFloat(0)
    let coordinate = CGPoint(x: x, y: y)
    return coordinate
}()

let fullScreenOrigin = CGPoint(x: 0, y: 0)

extension CallingVC {
    func animate() {
        switch self.stateVC {
        case .fullScreen:
            UIView.animate(withDuration: 0.5) {
                self.view.center = UIScreen.main.bounds.center
                self.view.transform = CGAffineTransform.identity
                self.view.layer.masksToBounds = true
                self.view.layer.cornerRadius = 0
                self.activeFullScreenView(active: true)
            }
        case .minimized:
            print("size: \(self.view.frame.size)")
            
            let trasform = CGAffineTransform.init(scaleX: outsideVideoSize.width/UIScreen.main.bounds.width, y: outsideVideoSize.height/UIScreen.main.bounds.height)

            UIView.animate(withDuration: 0.5) {
                self.view.center = CGPoint(x: outsideVideoOrigin.x + outsideVideoSize.width/2, y: outsideVideoOrigin.y + outsideVideoSize.height/2)
                self.view.transform = trasform
                self.view.layer.masksToBounds = true
                self.view.layer.cornerRadius = 12
                self.activeFullScreenView(active: false)
            }
        default:
            break
        }
    }
    
    func addTapAction() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        if self.stateVC == .minimized {
            self.stateVC = .fullScreen
            self.animate()
        }
    }
}
