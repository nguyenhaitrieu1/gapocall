//
//  CallingControlView.swift
//  GapoCallKit
//
//  Created by manh.le on 10/2/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit
import PureLayout
enum CallingActionType {
    case lound
    case mute
    case camera
    case rotate
    case decline
    case hangup
    case accept
}

class CallingControlView: UIView {
    lazy var contentStackView: UIStackView = {
        let contentStackView = UIStackView()
        contentStackView.axis = .horizontal
        contentStackView.alignment = .center
        contentStackView.distribution = .equalCentering
        return contentStackView
    }()
    
    lazy var loundButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-speaker-normal"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(loundAction), for: .touchUpInside)
        return button
    }()

    lazy var muteButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-mic-normal"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(muteAction), for: .touchUpInside)
        return button
    }()

    lazy var endButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-end-call"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(hangupAction), for: .touchUpInside)
        return button
    }()

    lazy var declineButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-cancel"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(declineAction), for: .touchUpInside)
        return button
    }()
    
    lazy var cameraButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-video-normal"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(cameraAction), for: .touchUpInside)
        return button
    }()
    
    lazy var rotateCameraButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-cam-rotate-normal"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(rotateAction), for: .touchUpInside)
        return button
    }()

    lazy var acceptAudioButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-accept"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(acceptAction), for: .touchUpInside)
        return button
    }()

    lazy var acceptVideoButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        button.setImage(UIImage(named: "ic-call-video-accept"), for: .normal)
        button.autoSetDimensions(to: CGSize(width: 64, height: 64))
        button.addTarget(self, action: #selector(acceptAction), for: .touchUpInside)
        return button
    }()

    var callingType: CallingType = .audio
    var isCalling: Bool = true
    var completion: ((CallingType, CallingActionType) -> ())?
    
    init(type: CallingType, state: SignalingState) {
        super.init(frame: .zero)
        self.callingType = type
        self.addSubview(contentStackView)
        contentStackView.autoPinEdgesToSuperviewMargins(with: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16))
        setSignalingState(state: state)
    }
    
    required init?(coder: NSCoder) {
        super.init(frame: .zero)
    }
    
    func setSignalingState(state: SignalingState) {
        isCalling = state == SignalingState.calling ? true : false
        updateUI()
    }
    
    func updateUI() {
        ///Remove All Subview
        self.contentStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        
        switch callingType {
        case .video:
            if isCalling {
                setupVideoCalling()
            } else {
                setupVideoAnswer()
            }
        default:
            if isCalling {
                setupAudioAnswer()
            } else {
                setupAudioCalling()
            }
        }
    }
    
    func setupAudioCalling() {
        //lound
        contentStackView.addArrangedSubview(loundButton)
        //mute
        contentStackView.addArrangedSubview(muteButton)
        //end
        contentStackView.addArrangedSubview(endButton)
    }
    
    func setupAudioAnswer() {
        //close
        contentStackView.addArrangedSubview(declineButton)
        //accept
        contentStackView.addArrangedSubview(acceptAudioButton)
    }
    
    func setupVideoCalling() {
        //close
        contentStackView.addArrangedSubview(declineButton)
        //accept
        contentStackView.addArrangedSubview(acceptVideoButton)
    }
    
    func setupVideoAnswer() {
        //video
        contentStackView.addArrangedSubview(cameraButton)
        //rotate
        contentStackView.addArrangedSubview(rotateCameraButton)
        //mute
        contentStackView.addArrangedSubview(muteButton)
        //end
        contentStackView.addArrangedSubview(endButton)
    }
    
    //MARK: - Actions
    
    @objc func loundAction() {
        completion?(callingType, .lound)
    }
    
    @objc func muteAction() {
        completion?(callingType, .mute)
    }
    
    @objc func hangupAction() {
        completion?(callingType, .hangup)
    }

    @objc func declineAction() {
        completion?(callingType, .decline)
    }

    @objc func cameraAction() {
        completion?(callingType, .camera)
    }

    @objc func rotateAction() {
        completion?(callingType, .rotate)
    }

    @objc func acceptAction() {
        completion?(callingType, .accept)
    }
}
