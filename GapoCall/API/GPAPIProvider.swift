//
//  GPAPIProvider.swift
//  GAPO
//
//  Created by toandk on 12/11/19.
//  Copyright © 2019 GAPO. All rights reserved.
//

import Foundation
import Moya
import DTMvvm
import Alamofire

class UserAlamofireManager: Alamofire.Session {
    static let shared: UserAlamofireManager = {
        let configuration = URLSessionConfiguration.default
//        configuration.httpAdditionalHeaders = Alamofire.Session.default
        configuration.timeoutIntervalForRequest = 5 // as seconds, you can set your request timeout
        configuration.timeoutIntervalForResource = 5 // as seconds, you can set your resource timeout
        configuration.requestCachePolicy = .useProtocolCachePolicy
        return UserAlamofireManager(configuration: configuration)
    }()
}

class GPAPIProvider<Target>: MoyaProvider<Target> where Target: TargetType {

    @discardableResult
    override func request(_ target: Target,
    callbackQueue: DispatchQueue? = .none,
    progress: ProgressBlock? = .none,
    completion: @escaping Completion) -> Cancellable {
        return super.request(target, callbackQueue: callbackQueue, progress: progress) { result in
            completion(result)
        }
    }
    
}
