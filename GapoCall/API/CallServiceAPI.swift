//
//  CallServiceAPI.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/2/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import Alamofire
import Moya
import RxSwift
import RxCocoa
import DTMvvm
import ObjectMapper

typealias CallResponse<T: Mappable> = (BaseAPIResponse<T>?, Error?) -> Void

let domain: String = "https://staging-api.gapo.vn/call-service/v1.0"

enum CallAPI {
    case setup
    case call(userId: String, type: CallType)
    case getCallInfo(callId: String)
    case hangup
}

extension CallAPI: TargetType {
    
    var baseURL: URL { return URL(string: domain)! }

    var path: String {
        switch self {
        case .setup:
            return "/api/setup"
        case .call:
            return "/api/call"
        case .getCallInfo(let callId):
            return "/api/call/\(callId)"
        case .hangup:
            return "/api/hangup"
        }
    }

    var method: Moya.Method {
        switch self {
        case .setup, .getCallInfo:
            return .get
        case .call:
            return .post
        case .hangup:
            return .delete
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .call(let userId, let type):
            let params: [String: Any] = ["target": userId,
                                         "call_type": type.rawValue]
            return .requestParameters(parameters: params, encoding: JSONEncoding.default)
        default:
            return .requestPlain
        }
    }

    var headers: [String: String]? {
        return ["Authorization": String(format: "Bearer %@", UserInfoManager.shared.user?.token ?? "")]
    }
    
    public var validationType: ValidationType {
      return .none
    }

}

class CallServiceAPI {

    private let provider = MoyaProvider<CallAPI>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .default))])
    
    func getServerInfo(completion: @escaping CallResponse<WSServer>) {
        provider.request(.setup) { (result) in
            do {
                let response = try result.get()
                guard let value = try response.mapJSON() as? [String : Any] else { return }
                guard let data = BaseAPIResponse<WSServer>(JSON: value) else { return }
                completion(data, nil)
            } catch {
                completion(nil, error)
                let printableError = error as CustomStringConvertible
                let errorMessage = printableError.description
                print(errorMessage)
            }
        }
    }
    
    func call(targetId: String, type: CallType, completion: @escaping CallResponse<WSServer>) {
        provider.request(.call(userId: targetId, type: type)) { (result) in
            do {
                let response = try result.get()
                guard let value = try response.mapJSON() as? [String : Any] else { return }
                guard let data = BaseAPIResponse<WSServer>(JSON: value) else { return }
                completion(data, nil)
            } catch {
                let printableError = error as CustomStringConvertible
                let errorMessage = printableError.description
                completion(nil, error)
                print(errorMessage)
            }
        }
    }
    
    func callInfo(callId: String, completion: @escaping CallResponse<CallResponseData>) {
        provider.request(.getCallInfo(callId: callId)) { (result) in
            do {
                let response = try result.get()
                guard let value = try response.mapJSON() as? [String : Any] else { return }
                guard let data = BaseAPIResponse<CallResponseData>(JSON: value) else { return }
                completion(data, nil)
            } catch {
                let printableError = error as CustomStringConvertible
                let errorMessage = printableError.description
                completion(nil, error)
                print(errorMessage)
            }
        }
    }
    
    func hangup() -> Single<BaseAPIResponse<WSServer>> {
        return provider.rx.request(.hangup).filterSuccessfulStatusAndRedirectCodes().mapObject(BaseAPIResponse<WSServer>.self)
    }
}
