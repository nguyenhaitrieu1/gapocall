//
//  AuthAPI.swift
//  GapoCall
//
//  Created by manh.le on 10/9/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import UIKit
import Moya

enum AuthAPI {
    case login(phone: String, password: String)
}

extension AuthAPI: TargetType {
    var headers: [String: String]? {
        return ["Authorization": "Bearer \(UserInfoManager.shared.user?.token ?? "")", "Content-Type": "application/json"]
    }

    var baseURL: URL {
        return URL(string: "https://staging-api.gapo.vn")!
    }

    var path: String {
        switch self {
            
        case .login:
            return "/auth/v2.0/login"
        }
    }

    var method: Moya.Method {
        return .post
    }

    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        
        var params: [String: Any] = [:]
        switch self {
        case .login(let phone, let password):
            params = [
                "device_id": "E3043392-B5B5-4BC0-A63F-8312A6BBDB6A",
                "client_id": "6n6rwo86qmx7u8aahgrq",
                "phone_number": phone,
                "password": password,
                "trusted_device": true]
        default:
            params = [:]
        }
        return .requestParameters(parameters: params, encoding: JSONEncoding.default)
    }
}

class AuthAPIService {
    
    private let provider = MoyaProvider<AuthAPI>(plugins: [NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
    
    func login(phone: String, password: String, completion: @escaping CallResponse<UserInfo>) {
        provider.request(.login(phone: phone, password: password)) { (result) in
            do {
                let response = try result.get()
                guard let value = try response.mapJSON() as? [String : Any] else { return }
                guard let data = BaseAPIResponse<UserInfo>(JSON: value) else { return }
                completion(data, nil)
            } catch {
                let printableError = error as CustomStringConvertible
                let errorMessage = printableError.description
                completion(nil, error)
                print(errorMessage)
            }
        }
    }

}
