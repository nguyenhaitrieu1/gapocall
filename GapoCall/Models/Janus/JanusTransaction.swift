//
//  JanusTransaction.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/6/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import Foundation

typealias TransactionCompletion = ([String: Any]?, Error?) -> Void

class JanusTransaction: NSObject {
    
    public var transactionId: String = ""
    var completion: TransactionCompletion?

}
