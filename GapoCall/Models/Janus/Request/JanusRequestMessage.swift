//
//  JanusRequestRegister.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import JanusGateway

struct JanusRequestMessage<CandidateRequest: Mappable>: Mappable {
    
    var janus: String = ""
    var transactionId: String = ""
    var handleId: UInt64 = 0
    var sessionId: UInt64 = 0
    var sender: UInt64 = 0
    var body: JanusRequestMessageBody?
    var jsep: JanusJsep?
    var candidate: CandidateRequest?
    
    init?(map: Map) { }
    
    init(transaction: String = Utilites.randomString(length: 12), sessionId: UInt64, handleId: UInt64, body: JanusRequestMessageBody? = nil, jsep: JanusJsep? = nil, candidate: CandidateRequest? = nil) {
        self.janus = "message"
        self.transactionId = transaction
        self.sessionId = sessionId
        self.handleId = handleId
        self.body = body
        self.jsep = jsep
        self.candidate = candidate
    }
    
    mutating func mapping(map: Map) {
        janus <- map["janus"]
        transactionId <- map["transaction"]
        handleId <- map["handle_id"]
        sessionId <- map["session_id"]
        body <- map["body"]
        jsep <- map["jsep"]
        candidate <- map["candidate"]
    }

}
