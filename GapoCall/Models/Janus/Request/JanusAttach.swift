//
//  JanusAttach.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import JanusGateway

enum JanusPluginType: String {
    
    case echo = "janus.plugin.echotest"
    case call = "janus.plugin.videocall"
    case room = "janus.plugin.videoroom"
    
}

struct JanusAttach: Mappable {
    
    var janus: String = ""
    var transactionId: String = ""
    var plugin: JanusPluginType = .call
    var sessionId: UInt64 = 0
    
    init?(map: Map) { }
    
    init(transaction: String = Utilites.randomString(length: 12), sessionId: UInt64, plugin: JanusPluginType = .call) {
        self.janus = "attach"
        self.transactionId = transaction
        self.sessionId = sessionId
        self.plugin = plugin
    }
    
    mutating func mapping(map: Map) {
        janus <- map["janus"]
        transactionId <- map["transaction"]
        plugin <- map["plugin"]
        sessionId <- map["session_id"]
    }

}
