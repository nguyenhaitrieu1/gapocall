//
//  JanusRequestMessageTrickle.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/9/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import WebRTC

struct JanusRequestCandidate: Mappable {
    
    var candidate: String = ""
    var sdpMid: String? = ""
    var sdpMLineIndex: Int32 = 0
    
    init?(map: Map) { }
    
    init(_ candidate: RTCIceCandidate) {
        self.candidate = candidate.sdp
        self.sdpMid = candidate.sdpMid
        self.sdpMLineIndex = candidate.sdpMLineIndex
    }
    
    mutating func mapping(map: Map) {
        candidate <- map["candidate"]
        sdpMid <- map["sdpMid"]
        sdpMLineIndex <- map["sdpMLineIndex"]
    }
    
}
