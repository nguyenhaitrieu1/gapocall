//
//  JanusCreate.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/6/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusCreate: Mappable {
    
    var janus: String = ""
    var transaction: String = ""
    
    init?(map: Map) { }
    
    init() { }
    
    mutating func mapping(map: Map) {
        janus <- map["janus"]
        transaction <- map["transaction"]
    }

}
