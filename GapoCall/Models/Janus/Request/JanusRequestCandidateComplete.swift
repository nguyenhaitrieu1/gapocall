//
//  JanusRequestCandidateComplete.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/12/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusRequestCandidateComplete: Mappable {
    
    init?(map: Map) { }
    
    var completed: Bool = false
    
    init(_ completed: Bool = true) {
        self.completed = completed
    }
    
    mutating func mapping(map: Map) {
        completed <- map["completed"]
    }
    
}
