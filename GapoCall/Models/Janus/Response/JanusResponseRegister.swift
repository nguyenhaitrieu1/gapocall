//
//  JanusResponseRegister.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusResponseRegister: Mappable {
    
    init?(map: Map) { }
    
    var plugin: JanusPluginType = .echo
    
    mutating func mapping(map: Map) {
        plugin <- map["plugin"]
    }
    
}
