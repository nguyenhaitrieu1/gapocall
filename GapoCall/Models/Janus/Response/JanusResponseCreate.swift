//
//  JanusResponseCreate.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusResponseCreate: Mappable {
    
    init?(map: Map) { }
    
    var sessionId: UInt64 = 0
    
    mutating func mapping(map: Map) {
        sessionId <- map["id"]
    }
    
}
