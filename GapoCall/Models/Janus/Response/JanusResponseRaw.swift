//
//  JanusResponseRaw.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusResponseRaw: JanusResponse, Mappable {
    
    init?(map: Map) { }
    
    var status: JanusResponseStatus = .success
    
    var transactionId: String = ""
    var sessionId: UInt64?
    var sender: UInt64?
    var plugingData: PluginData?
    var jsep: JanusJsep?
    mutating func mapping(map: Map) {
        baseMapping(map: map)
        sessionId <- map["session_id"]
        sender <- map["sender"]
        plugingData <- map["plugindata"]
        jsep <- map["jsep"]
    }
    
}
