//
//  JanusResponse.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

struct JanusResponseBase<JanusData: Mappable>: Mappable, JanusResponse {
    
    init?(map: Map) { }
    
    var status: JanusResponseStatus = .success
    var transactionId: String = ""
    var sender: UInt64 = 0
    var data: JanusData?
    var pluginData: JanusData?
    var jsep: JanusJsep?
    
    mutating func mapping(map: Map) {
        baseMapping(map: map)
        data <- map["data"]
        pluginData <- map["plugindata"]
        jsep <- map["jsep"]
    }
    
}
