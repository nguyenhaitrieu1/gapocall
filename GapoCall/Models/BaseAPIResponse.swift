//
//  BaseAPIResponse.swift
//  GAPO
//
//  Created by ToanDK on 8/29/19.
//  Copyright © 2019 Tuan Anh Tran. All rights reserved.
//

import Foundation
import ObjectMapper
import DTMvvm

enum ResponseCode: Int {
    case success = 1
    case fail = 0
    case maxSeeFirstError = 1400
}

class RelationAPIResponse: Model {
    var code: String = ""
    var statusCode: Int = 200
    var message: String = ""
    var data: Int = 0
    var count: Int = 0

    convenience init() {
        self.init(JSON: [String: Any]())!
    }

    override func mapping(map: Map) {
        code <- map["code"]
        statusCode <- map["code"]
        message <- map["message"]
        data <- map["data"]
        count <- map["data"]
    }
}

class BaseAPIResponse<T: Mappable>: Model {
    var code: ResponseCode = .success
    var statusCode: Int = 200
    var message: String = ""
    var data: T?
    var count: Int = 0
    var internalCode: String?

    convenience init() {
        self.init(JSON: [String: Any]())!
    }

    override func mapping(map: Map) {
        code <- (map["code"], EnumTransform<ResponseCode>())
        statusCode <- map["code"]
        message <- map["message"]
        data <- map["data"]
        count <- map["data"]
        internalCode <- (map["error_code"])
    }
}

class BaseListAPIResponse<T: Mappable>: Model {
    var code: ResponseCode = .success
    var message: String = ""
    var data: [T]?
    var nextLink: String = ""
    var previousLink: String = ""
	var totalItems: Int = 0
	var totalPages: Int = 0
    var itemCount: Int = 0

    convenience init() {
        self.init(JSON: [String: Any]())!
    }

    override func mapping(map: Map) {
        code <- (map["code"], EnumTransform<ResponseCode>())
        message <- map["message"]
        data <- map["data"]
        nextLink <- (map["links.next"])
        nextLink = nextLink.replacingOccurrences(of: "?", with: "")
        previousLink <- (map["links.previous"])
		totalItems <- (map["links.total_items"])
		totalPages <- (map["links.total_pages"])
        itemCount <- (map["links.item_count"])
    }
}

//class BaseListDictAPIResponse<T: DictMappable>: Model {
//    var code: ResponseCode = .success
//    var message: String = ""
//    var data: [T]?
//
//    convenience init() {
//        self.init(JSON: [String: Any]())!
//    }
//
//    override func mapping(map: Map) {
//        code <- (map["code"], EnumTransform<ResponseCode>())
//        message <- map["message"]
//        data <- (map["data"], ListDictTransform<T>())
//    }
//}
