//
//  UserInfo.swift
//  GapoCall
//
//  Created by manh.le on 10/8/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import DTMvvm
import ObjectMapper

class UserInfo: Mappable {    
    var userId: Int?
    var token: String?
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        userId <- map["user_id"]
        token <- map["access_token"]
    }
    
    func saveConfig(key: String) {
        AppConfig.setCacheDictionary(value: self.toJSON(), key: key)
    }
    
    static func loadFromCache(key: String) -> UserInfo? {
        guard let data = AppConfig.getCacheDictionary(key: key) else { return nil}
        return UserInfo(JSON: data)
    }

}
