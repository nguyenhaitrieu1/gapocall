//
//  TurnServer.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import WebRTC

class TurnServer: Mappable {
    
    var uri: String = ""
    var port: Int = 0
    var type: String = ""
    var user: String = ""
    var password: String = ""
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        uri <- map["uri"]
        port <- map["port"]
        type <- map["type"]
        user <- map["user"]
        password <- map["pwd"]
    }
    
}

extension TurnServer {
    
    private func genURL() -> String {
        return "turn:\(uri):\(port)?transport=\(type)"
    }
    
    func toIceServers() -> RTCIceServer {
        let iceServer = RTCIceServer(urlStrings: [genURL()], username: user, credential: password)
        return iceServer
    }
    
}
