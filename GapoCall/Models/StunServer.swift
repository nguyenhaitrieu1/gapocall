//
//  StunServer.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import WebRTC

class StunServer: Mappable {
    
    var uri: String = ""
    var port: Int = 0
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        uri <- map["uri"]
        port <- map["port"]
    }
    
}

extension StunServer {
    
    private func genURL() -> String {
        return "stun:\(uri):\(port)"
    }
    
    func toIceServers() -> RTCIceServer {
        let iceServer = RTCIceServer(urlStrings: [genURL()])
        return iceServer
    }
    
}
