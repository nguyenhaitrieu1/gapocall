//
//  CallResponseData.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

class CallResponseData: Mappable {
    
    var info: CallInfo?
    var serverInfo: WSServer?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        serverInfo <- map["server_info"]
        info <- map["call"]
    }
}
