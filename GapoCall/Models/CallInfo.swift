//
//  CallInfo.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

enum CallType: Int {
    case audio = 0
    case video = 1
}

enum CallStatus: Int {
    case `init` = 0
    case incoming = 1
    case calling = 2
    case hangup = 3
    case adminHangup = 4
}

class CallInfo: Mappable {
    
    var owner: Int = 0
    var target: Int = 0
    var type: CallType = .video
    var status: CallStatus = .`init`
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        owner <- map["owner"]
        target <- map["target"]
        type <- map["call_type"]
        status <- map["status"]
    }

}
