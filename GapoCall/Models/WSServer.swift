//
//  WSServer.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

class WSServer: Mappable {
    
    var uri: String = ""
    var id: String = ""
    var token: String = ""
    var userId: UInt64 = 0
    var turnServers: [TurnServer] = []
    var stunServers: [StunServer] = []
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        uri <- map["server_uri"]
        id <- map["servcer_id"]
        token <- map["token"]
        userId <- map["user_id"]
        turnServers <- map["turn_server"]
        stunServers <- map["stun_server"]
    }
    

}
