//
//  GapoRTCClient.swift
//  GapoCall
//
//  Created by manh.le on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import Foundation
import WebRTC

protocol GCallClientDelegate: class {
    func rtcClient(_ client: GCallClient, didDiscoverLocalCandidate candidate: RTCIceCandidate)
    func rtcClient(_ client: GCallClient, didChangeConnectionState state: RTCIceConnectionState)
    func rtcClient(_ client: GCallClient, didChangeConnectionState state: RTCIceGatheringState)
    func rtcClient(_ client: GCallClient, didReceiveData data: Data)
    func rtcClient(client : GCallClient, didReceiveError error: Error)
}

extension GCallClientDelegate {
    // add default implementation to extension for optional methods
    func rtcClient(client : GCallClient, didReceiveError error: Error) {

    }
}

protocol GClientVideoViewDelegate: class {
    func rtcClient(client : GCallClient, didReceiveLocalVideoView localVideoView: RTCVideoRenderer)
    func rtcClient(client : GCallClient, didReceiveRemoteVideoView remoteVideoView: RTCEAGLVideoView)
}

class GCallClient: NSObject {
    
    var wsServer: WSServer? {
        didSet {
            self.createMediaSenders()
            self.configureAudioSession()
            self.peerConnection?.delegate = self
        }
    }
    
    static let factory: RTCPeerConnectionFactory = {
        let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
        let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    
    lazy var iceServers: [RTCIceServer] = {
        guard let server = wsServer else { return [] }
        var iceServers: [RTCIceServer] = []
        for turnServer in server.turnServers {
            iceServers.append(turnServer.toIceServers())
        }
        for stunServer in server.stunServers {
            iceServers.append(stunServer.toIceServers())
        }
        return iceServers
    }()
    
    lazy var defaultPeerConnectionConstraints: RTCMediaConstraints = {
        let optionalConstraints: [String: String] = ["DtlsSrtpKeyAgreement": kRTCMediaConstraintsValueTrue]
        let constrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: optionalConstraints)
        return constrains
    }()
    
    lazy var config: RTCConfiguration = {
        var config: RTCConfiguration = RTCConfiguration()
        config.iceServers = self.iceServers
        config.tcpCandidatePolicy = .enabled
        config.bundlePolicy = .maxBundle
        config.enableDscp = true
        config.iceTransportPolicy = .relay
        config.continualGatheringPolicy = .gatherContinually
        return config
    }()
    
    lazy var peerConnection: RTCPeerConnection? = {
        var peerConnection: RTCPeerConnection = GCallClient.factory.peerConnection(with: self.config, constraints: self.defaultPeerConnectionConstraints, delegate: self)
        return peerConnection
    }()
    
    weak var delegate: GCallClientDelegate?
    weak var videoViewDelegate: GClientVideoViewDelegate? {
        didSet {
            if remoteVideoTrack != nil {
                DispatchQueue.main.async {
                    self.videoViewDelegate?.rtcClient(client: self, didReceiveRemoteVideoView: self.remoteRenderView!)
                }
            }
            
            if let localVideoRenderer = localVideoRenderer {
                self.videoViewDelegate?.rtcClient(client: self, didReceiveLocalVideoView: localVideoRenderer)
            }
        }
    }

    private let rtcAudioSession =  RTCAudioSession.sharedInstance()
    private let audioQueue = DispatchQueue(label: "audio")
        
    private let audioCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true"],
                                                          optionalConstraints: nil)
    private let videoCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true",
                                                                                 "OfferToReceiveVideo": "true"],
                                                          optionalConstraints: nil)
    var mediaConstrains : RTCMediaConstraints {
        return self.callType == .video ? self.videoCallConstraint : self.audioCallConstraint
    }

    lazy var currentMediaConstraint: [String: String] = {
        let constraint = ["":""]
        return constraint
    }()
    
    private var videoCapturer: RTCVideoCapturer?
    private var localVideoTrack: RTCVideoTrack?
    private var localVideoRenderer: RTCVideoRenderer?

    private var localAudioTrack: RTCAudioTrack?
    
    private var remoteVideoTrack: RTCVideoTrack?
    private var localDataChannel: RTCDataChannel?
    private var remoteDataChannel: RTCDataChannel?
    private lazy var remoteRenderView: RTCEAGLVideoView? = {
        let remoteRenderView = RTCEAGLVideoView(frame: .zero)
        remoteRenderView.delegate = self
        return remoteRenderView
    }()
        
    var callType: CallType = .video
    //MARK: - Init
    override init() {
        super.init()
    }
    
    init(type: CallType = .video) {
        super.init()
        self.callType = type
    }
    
    deinit {
        let className = NSStringFromClass(self.classForCoder)
        print("~> \(className) did release...")

        if let stream = peerConnection?.localStreams.first {
            localAudioTrack = nil
            peerConnection?.remove(stream)
            closeConnection()
        }
    }
    
    func closeConnection() {
        peerConnection?.close()
        peerConnection = nil
    }
    
    // MARK: Signaling
    func offer(completion: @escaping (_ sdp: RTCSessionDescription) -> Void) {
        self.peerConnection?.offer(for: mediaConstrains) { (sdp, error) in
            if let error = error {
                self.delegate?.rtcClient(client: self, didReceiveError: error)
                return
            }
            guard let sdp = sdp else { return }
            self.peerConnection?.setLocalDescription(sdp, completionHandler: { (error) in
                if let error = error {
                    self.delegate?.rtcClient(client: self, didReceiveError: error)
                }
                completion(sdp)
            })
        }
    }
    
    func answer(completion: @escaping (_ sdp: RTCSessionDescription) -> Void)  {
        self.peerConnection?.answer(for: mediaConstrains) { (sdp, error) in
            if let error = error {
                self.delegate?.rtcClient(client: self, didReceiveError: error)
                return
            }
            guard let sdp = sdp else {
                return
            }
            self.peerConnection?.setLocalDescription(sdp, completionHandler: { (error) in
                if let error = error {
                    self.delegate?.rtcClient(client: self, didReceiveError: error)
                }
                completion(sdp)
            })
        }
    }
    
    func set(remoteSdp: RTCSessionDescription, completion: @escaping (Error?) -> ()) {
        self.peerConnection?.setRemoteDescription(remoteSdp, completionHandler: completion)
    }
    
    func set(remoteCandidate: RTCIceCandidate) {
        self.peerConnection?.add(remoteCandidate)
    }
}

extension GCallClient {
    // MARK: Media
    func startCaptureLocalVideo(renderer: RTCVideoRenderer) {
        guard let capturer = self.videoCapturer as? RTCCameraVideoCapturer else {
            return
        }
        
        guard
            let frontCamera = (RTCCameraVideoCapturer.captureDevices().first { $0.position == .front }),
            
            // choose highest res
            let format = (RTCCameraVideoCapturer.supportedFormats(for: frontCamera).sorted { (f1, f2) -> Bool in
                let width1 = CMVideoFormatDescriptionGetDimensions(f1.formatDescription).width
                let width2 = CMVideoFormatDescriptionGetDimensions(f2.formatDescription).width
                return width1 < width2
            }).first,
            
            // choose highest fps
            let fps = (format.videoSupportedFrameRateRanges.sorted { return $0.maxFrameRate < $1.maxFrameRate }.first) else {
            return
        }
        
        capturer.startCapture(with: frontCamera,
                              format: format,
                              fps: Int(fps.maxFrameRate))
        localVideoRenderer = renderer
        self.localVideoTrack?.add(renderer)
        self.videoViewDelegate?.rtcClient(client: self, didReceiveLocalVideoView: renderer)
    }
        
    func createMediaSenders() {
        let streamId = "stream"
        
        // Audio
        self.localAudioTrack = self.createAudioTrack()
        if let senderAudio: RTCRtpSender = self.peerConnection?.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: streamId) {
            senderAudio.track = self.localAudioTrack
        }
        
        // Video
        self.localVideoTrack = self.createVideoTrack()
        if let senderVideo: RTCRtpSender = self.peerConnection?.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: streamId) {
            senderVideo.track = self.localVideoTrack
        }
        
        #if arch(arm64)
            // Using metal (arm64 only)
            let localRenderer = RTCMTLVideoView(frame: CGRect.zero)
            localRenderer.videoContentMode = .scaleAspectFill
        #else
            // Using OpenGLES for the rest
            let localRenderer = RTCEAGLVideoView(frame: CGRect.zero)
        #endif

        startCaptureLocalVideo(renderer: localRenderer)
    }

    private func configureAudioSession() {
        self.rtcAudioSession.lockForConfiguration()
        do {
            try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
            try self.rtcAudioSession.setMode(AVAudioSession.Mode.voiceChat.rawValue)
        } catch let error {
            debugPrint("Error changeing AVAudioSession category: \(error)")
        }
        self.rtcAudioSession.unlockForConfiguration()
    }
    
    
    private func createAudioTrack() -> RTCAudioTrack {
        let audioConstrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let audioSource = GCallClient.factory.audioSource(with: audioConstrains)
        let audioTrack = GCallClient.factory.audioTrack(with: audioSource, trackId: "audio0")
        return audioTrack
    }
    
    private func createVideoTrack() -> RTCVideoTrack {
        let videoSource = GCallClient.factory.videoSource()
        
        #if TARGET_OS_SIMULATOR
        self.videoCapturer = RTCFileVideoCapturer(delegate: videoSource)
        #else
        self.videoCapturer = RTCCameraVideoCapturer(delegate: videoSource)
        #endif
        
        let videoTrack = GCallClient.factory.videoTrack(with: videoSource, trackId: "video0")
        return videoTrack
    }
}

// MARK:- Audio control

extension GCallClient {
    func muteAudio() {
        self.setAudioEnabled(false)
    }
    
    func unmuteAudio() {
        self.setAudioEnabled(true)
    }
    
    // Fallback to the default playing device: headphones/bluetooth/ear speaker
    func speakerOff() {
        self.audioQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.none)
            } catch let error {
                debugPrint("Error setting AVAudioSession category: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    
    // Force speaker
    func speakerOn() {
        self.audioQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.speaker)
                try self.rtcAudioSession.setActive(true)
            } catch let error {
                debugPrint("Couldn't force audio to speaker: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    
    private func setAudioEnabled(_ isEnabled: Bool) {
        guard let audioTracks = self.peerConnection?.transceivers.compactMap({ return $0.sender.track as? RTCAudioTrack }) else { return }
            audioTracks.forEach { $0.isEnabled = isEnabled }
    }
}

extension GCallClient: RTCPeerConnectionDelegate {
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        debugPrint("peerConnection new signaling state: \(stateChanged)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        debugPrint("peerConnection did add stream")
        
        if let track = stream.videoTracks.first {
            DispatchQueue.main.async {
                self.videoViewDelegate?.rtcClient(client: self, didReceiveRemoteVideoView: self.remoteRenderView!)
                track.add(self.remoteRenderView!)
                self.remoteVideoTrack = track
            }
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        debugPrint("peerConnection did remote stream")
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        debugPrint("peerConnection should negotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        debugPrint("peerConnection new connection state: \(newState)")
        self.delegate?.rtcClient(self, didChangeConnectionState: newState)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        debugPrint("peerConnection new gathering state: \(newState)")
        self.delegate?.rtcClient(self, didChangeConnectionState: newState)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        self.delegate?.rtcClient(self, didDiscoverLocalCandidate: candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        debugPrint("peerConnection did remove candidate(s)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        debugPrint("peerConnection did open data channel")
        self.remoteDataChannel = dataChannel
    }
}

// MARK: Data Channels

extension GCallClient: RTCDataChannelDelegate {
    private func createDataChannel() -> RTCDataChannel? {
        let config = RTCDataChannelConfiguration()
        guard let dataChannel = self.peerConnection?.dataChannel(forLabel: "WebRTCData", configuration: config) else {
            debugPrint("Warning: Couldn't create data channel.")
            return nil
        }
        return dataChannel
    }
    
    func sendData(_ data: Data) {
        let buffer = RTCDataBuffer(data: data, isBinary: true)
        self.remoteDataChannel?.sendData(buffer)
    }
    
    func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        debugPrint("dataChannel did change state: \(dataChannel.readyState)")
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        self.delegate?.rtcClient(self, didReceiveData: buffer.data)
    }
}

extension GCallClient: RTCVideoViewDelegate {
    func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        print("video did change size \(size)")
    }
}

public struct ErrorDomain {
    public static let videoPermissionDenied = "Video permission denied"
    public static let audioPermissionDenied = "Audio permission denied"
}
