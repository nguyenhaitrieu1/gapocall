//
//  CallService+Client.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/9/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import WebRTC

extension CallService: GCallClientDelegate {
    func rtcClient(_ client: GCallClient, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
        guard let handleId = handleId else { return }
        webRTCClient?.trickleCandidate(handleId: handleId, candidate: candidate)
    }
    
    func rtcClient(_ client: GCallClient, didChangeConnectionState state: RTCIceConnectionState) {
        
    }
    
    func rtcClient(_ client: GCallClient, didChangeConnectionState state: RTCIceGatheringState) {
        guard let handleId = handleId else { return }
        webRTCClient?.trickleCandidateComplete(handleId: handleId)
    }
    
    func rtcClient(_ client: GCallClient, didReceiveData data: Data) {
        
    }
}
