//
//  UserInfoManager.swift
//  GapoCall
//
//  Created by manh.le on 10/9/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import Foundation

class UserInfoManager: NSObject {
    static let shared = UserInfoManager()
    var user: UserInfo?
    var incomingTargetId: String?
    var outgoingTargetId: String?
}
