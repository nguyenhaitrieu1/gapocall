
import Foundation
import WebRTC
import SocketRocket
import JanusGateway
import ObjectMapper

enum SignalingChannelState {
    case close
    case open
    case create
    case attach
    case join
    case offer
    case error
}

protocol WebRTCClientDelegate: class {
    
    func webRTCClient(_ client: WebRTCClient, onAttached handleId: UInt64)
    
    func webRTCClient(didRegisted client: WebRTCClient)
    
    func webRTCClient(_ client: WebRTCClient, onPublisherRemoteJsep handleId: Int, jsep: [String: Any])
    
    func webRTCClient(_ client: WebRTCClient, subscriberRemote jsep: JanusJsep)
    
    func webRTCClient(_ client: WebRTCClient, onLeaving handleId: Int)
    func webRTCClient(incoming client: WebRTCClient, subscriberRemote jsep: JanusJsep)
}

final class WebRTCClient: NSObject {
    
    // The `RTCPeerConnectionFactory` is in charge of creating new RTCPeerConnection instances.
    // A new RTCPeerConnection should be created every new call, but the factory is shared.
    private static let factory: RTCPeerConnectionFactory = {
        RTCInitializeSSL()
        let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
        let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
        return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
    }()
    
    weak var delegate: WebRTCClientDelegate?
    
    var state: SignalingChannelState! = .close
    
    var url: URL!
    var socket: SRWebSocket!
    var sessionId: UInt64 = 0
    var handleId: UInt64 = 0
    var keepAliveTimer: Timer?
    var transactionHash: [String: JanusTransaction] = [:]
    
    deinit {
        let className = NSStringFromClass(self.classForCoder)
        print("~> \(className) did release...")
        self.disconnect()
        stopTimer()
    }
    
    convenience init(url: URL) {
        self.init()
        self.url = url
        let protocols: [String] = ["janus-protocol"]
        self.socket = SRWebSocket(url: url, protocols: protocols)
        self.socket.delegate = self
        keepAliveTimer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { [weak self] (timer) in
            self?.keepAlive()
        })
        self.socket.open()
    }
    
    func createSession() {
        let transactionId: String = Utilites.randomString(length: 12)
        var createParams: JanusCreate = JanusCreate()
        createParams.janus = "create"
        createParams.transaction = transactionId
        let transaction: JanusTransaction = JanusTransaction()
        transaction.transactionId = transactionId
        transaction.completion = { [weak self] (response, error) in
            guard let json = response, let janusResponse = JanusResponseBase<JanusResponseCreate>(JSON: json) else { return }
            guard let sessionId = janusResponse.data?.sessionId else { return }
            self?.sessionId = sessionId
            self?.attachPlugin()
        }
        transactionHash[transactionId] = transaction
        send(createParams)
    }
    
    func attachPlugin() {
        let request: JanusAttach = JanusAttach(sessionId: self.sessionId)
        let transaction: JanusTransaction = JanusTransaction()
        transaction.transactionId = request.transactionId
        transaction.completion = { [weak self] (response, error) in
            guard let json = response, let janusResponse = JanusResponseBase<JanusResponseAttach>(JSON: json) else { return }
            guard let handleId = janusResponse.data?.handleId else { return }
            guard let self = self else { return }
            self.handleId = handleId
            self.delegate?.webRTCClient(self, onAttached: handleId)
            print("attach handleId \(handleId)")
        }
        self.transactionHash[request.transactionId] = transaction
        send(request)
    }
    
    func register(token: String, handleId: UInt64, userId: UInt64) {
        let body: JanusRequestMessageBody = JanusRequestMessageBody(userId: "\(userId)", sessionId: self.sessionId, token: token)
        body.request = .register
        let request: JanusRequestMessage<JanusRequestMessageBody> = JanusRequestMessage(sessionId: self.sessionId, handleId: handleId, body: body)
        let transaction: JanusTransaction = JanusTransaction()
        transaction.transactionId = request.transactionId
        transaction.completion = { [weak self] (response, error) in
            guard let json = response, let janusResponse = JanusResponseBase<JanusResponseRegister>(JSON: json) else { return }
            guard let plugin = janusResponse.pluginData?.plugin else { return }
            guard let self = self else { return }
            print("janus register success \(plugin.rawValue)")
            self.delegate?.webRTCClient(didRegisted: self)
        }
        self.transactionHash[request.transactionId] = transaction
        send(request)
    }
    
    func call(sdp: RTCSessionDescription, token: String, targetId: String, handleId: UInt64) {
        let body: JanusRequestMessageBody = JanusRequestMessageBody(userId: targetId, sessionId: self.sessionId, token: token)
        body.request = .call
        body.userId = targetId
        let jsep: JanusJsep = JanusJsep(sdp: sdp)
        var request: JanusRequestMessage<JanusRequestCandidate> = JanusRequestMessage<JanusRequestCandidate>(sessionId: self.sessionId, handleId: handleId, body: body)
        request.jsep = jsep
        send(request)
    }
    
    func trickleCandidate(handleId: UInt64, candidate: RTCIceCandidate) {
        let jnCandidate: JanusRequestCandidate = JanusRequestCandidate(candidate)
        var request: JanusRequestMessage<JanusRequestCandidate> = JanusRequestMessage(sessionId: self.sessionId, handleId: handleId, candidate: jnCandidate)
        request.janus = "trickle"
        request.candidate = jnCandidate
        send(request)
    }
    
    func trickleCandidateComplete(handleId: UInt64) {
        let jnCandidate: JanusRequestCandidateComplete = JanusRequestCandidateComplete()
        var request: JanusRequestMessage<JanusRequestCandidateComplete> = JanusRequestMessage(sessionId: self.sessionId, handleId: handleId)
        request.janus = "trickle"
        request.candidate = jnCandidate
        send(request)
    }
    
    func answer(targetId: String, sdp: RTCSessionDescription) {
        let body: JanusRequestMessageBody = JanusRequestMessageBody(userId: targetId, sessionId: self.sessionId)
        body.request = .accept
        let jsep: JanusJsep = JanusJsep(sdp: sdp)
        let request: JanusRequestMessage<JanusRequestMessageBody> = JanusRequestMessage(sessionId: self.sessionId, handleId: handleId, body: body, jsep: jsep)
        send(request)
    }
    
    func hangup(targetId: String) {
        let body: JanusRequestMessageBody = JanusRequestMessageBody(userId: targetId, sessionId: self.sessionId)
        body.request = .hangup
        let request: JanusRequestMessage<JanusRequestMessageBody> = JanusRequestMessage(sessionId: self.sessionId, handleId: handleId, body: body, jsep: nil)
        send(request)
    }
    
    private func keepAlive() {
        let transaction: String = Utilites.randomString(length: 12)
        let pingParams: JanusKeepAlive = JanusKeepAlive(transaction: transaction, sessionId: sessionId)
        send(pingParams)
    }
    
    private func send<T: Mappable> (_ params: T) {
        guard let json = params.toJSONString() else {
            print("Bad json for request")
            return
        }
        print("outgoing data \(json)")
        socket.send(json)
    }
    
    func disconnect() {
        if self.state == .close || self.state == .error {
            return
        }
        socket.close()
    }

    func stopTimer() {
        keepAliveTimer?.invalidate()
        keepAliveTimer = nil
    }
}

extension WebRTCClient: SRWebSocketDelegate {
    
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
        print("didReceiveMessage \(String(describing: message))")
        if let string = message as? String, let data = string.data(using: .utf8) {
            do {
                if let JSON = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String: Any]
                {
                    guard let response: JanusResponseRaw = JanusResponseRaw(JSON: JSON) else { return }
                    switch response.status {
                    case .success, .ack, .error:
                        guard let transaction: JanusTransaction = self.transactionHash[response.transactionId] else { return }
                        self.transactionHash.removeValue(forKey: response.transactionId)
                        transaction.completion?(JSON, nil)
                    case .event:
                        switch response.plugingData?.result?.event {
                        case .registered:
                            self.delegate?.webRTCClient(didRegisted: self)
                        case .incomingcall:
                            if let jsep = response.jsep {
                                self.delegate?.webRTCClient(incoming: self, subscriberRemote: jsep)
                            }
                        case .accepted:
                            if let jsep = response.jsep {
                                self.delegate?.webRTCClient(self, subscriberRemote: jsep)
                            }
                        default:
                            break
                        }
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
        print("webSocketDidOpen")
        self.state = .open
        createSession()
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!) {
        self.state = .error
        print("didFailWithError")
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
        print("didCloseWithCode")
        self.state = .close
        self.stopTimer()
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didReceivePong pongPayload: Data!) {
        print("didReceivePong")
    }

}
