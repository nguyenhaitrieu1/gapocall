//
//  CallService+WebRTCClient.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import JanusGateway

extension CallService: WebRTCClientDelegate {
    
    func webRTCClient(_ client: WebRTCClient, onAttached handleId: UInt64) {
        self.handleId = handleId
        guard let serverToken = wsServer?.token, let userId = wsServer?.userId else { return }
        client.register(token: serverToken, handleId: handleId, userId: userId)
    }
    
    func webRTCClient(didRegisted client: WebRTCClient) {
        call { (response, error) in }
    }
    
    func webRTCClient(_ client: WebRTCClient, onPublisherRemoteJsep handleId: Int, jsep: [String : Any]) {
        
    }
    
    func webRTCClient(_ client: WebRTCClient, subscriberRemote jsep: JanusJsep) {
        print("set remote jsep")
        self.callClient?.set(remoteSdp: jsep.toSdp(), completion: { (error) in
            print("result remote jsep: ")
        })
    }
    
    func webRTCClient(_ client: WebRTCClient, onLeaving handleId: Int) {
        
    }
    
    func webRTCClient(incoming client: WebRTCClient, subscriberRemote jsep: JanusJsep) {
        print("set remote jsep")
        self.callClient?.set(remoteSdp: jsep.toSdp(), completion: {[weak self] (_) in
            self?.onAnswer()
        })
    }
    
    func onAnswer() {
        guard let callInfo = self.callInfo else { return }
        self.callClient?.answer(completion: { (sdp) in
            self.webRTCClient?.answer(targetId: "\(callInfo.target)", sdp: sdp)
        })

    }
    
    func createOffer() {
        self.callClient?.offer(completion: { [weak self] (sdp) in
            guard let self = self else { return }
            guard let serverToken = self.wsServer?.token, let handleId = self.handleId else { return }
            self.webRTCClient?.call(sdp: sdp, token: serverToken, targetId: self.targetId, handleId: handleId)
        })
    }

}
