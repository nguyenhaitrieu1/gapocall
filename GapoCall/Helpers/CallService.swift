//
//  CallService.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import DTMvvm
import RxCocoa
import RxSwift
import Alamofire
import WebRTC
import GapoMQTT

class CallService: NSObject {

    var targetId: String = ""
    var type: CallType
    var sdp: RTCSessionDescription?
    var callId: String?
    var callInfo: CallInfo?
    
    private let callAPI: CallServiceAPI
    
    private let disposeBag: DisposeBag
    
    private let stuns: [StunServer] = []
    private let turns: [TurnServer] = []
    
    var wsServer: WSServer?
    
    var webRTCClient: WebRTCClient?
    var handleId: UInt64?
    var callClient: GCallClient? {
        didSet {
            callClient?.delegate = self
        }
    }
    
    var onSetupFinish: (() -> Void)?
    
    init(outgoing targetId: String, type: CallType) {
        self.targetId = targetId
        self.type = type
        self.callAPI = CallServiceAPI()
        self.disposeBag = DisposeBag()
        super.init()
        self.handleMQTT()
        self.callClient = GCallClient(type: type)
        setup()
    }
    
    init(inComing callId: String, type: CallType) {
        self.type = type
        self.callAPI = CallServiceAPI()
        self.disposeBag = DisposeBag()
        self.callId = callId
        super.init()
        self.callClient = GCallClient(type: type)
    }
    
    deinit {
        let className = NSStringFromClass(self.classForCoder)
        print("~> \(className) did release...")
    }

    public func handleMQTT() {
        MQTTManager.shared.rxOfferEvent.subscribe(onNext: { [weak self] (offer) in
            self?.createOffer()
        }) => disposeBag        
    }
    
    public func setup() {
        callAPI.getServerInfo { [weak self] (response, error) in
            guard let wsServer = response?.data else { return }
            self?.wsServer = wsServer
            self?.connectWS(with: wsServer)
            self?.callClient?.wsServer = wsServer
            self?.onSetupFinish?()
            print("server info \(String(describing: response?.data))")
        }
    }
    
    private func connectWS(with server: WSServer) {
        guard let wsURL = URL(string: server.uri) else { return }
        webRTCClient = WebRTCClient(url: wsURL)
        webRTCClient?.delegate = self
    }
    
    func call(completion: @escaping CallResponse<WSServer>) {
        callAPI.call(targetId: targetId, type: type) { (response, error) in
            completion(response, error)
            if let error = error {
                print(error)
            }
        }
    }
    
    /**
     Luồng Answer:
     + Sau khi nhận được MQTT có cuộc gọi tới -> Nếu click Answer -> Gọi api CallInfo để lấy thông tin server
     + Có thông tin server trả về -> Join vào Janus
     + Sau khi join vào janus: Create -> Attach -> Register thành công -> lấy thông tin jsep từ Socket trả về -> gửi tới WebRTC
     */
    func createAnswer() {
        guard let callId = self.callId else { return }
        callAPI.callInfo(callId: callId) {[weak self]  (response, error) in
            guard let data = response?.data, let wsServer = data.serverInfo, let callInfo = data.info else { return }
            self?.callInfo = callInfo
            self?.wsServer = wsServer
            self?.connectWS(with: wsServer)
            self?.callClient?.wsServer = wsServer
            self?.onSetupFinish?()
        }
    }
    
    /// +Nếu chưa kết nối cuộc gọi thành công -> gọi API để bắn event MQTT cho máy gọi tới nhận được trạng thái kết thúc cuộc gọi
    /// +Nếu đã có kết nối thành công giữa 2 device -> Chỉ cần gọi hang up janus
    func hangup(completion: @escaping (Error?) -> Void) {
        webRTCClient?.hangup(targetId: targetId)
        callClient?.closeConnection()
        // Todo: Thêm state để case trường hợp trước và sau connected
        callAPI.hangup().subscribe(onSuccess: { [weak self] response in
            print("hang_up_result: \(response)")
            completion(nil)
        }, onError: { [weak self] (error) in
            print("hang_up_error: \(error)")
            completion(error)
        }) => disposeBag
    }
}
