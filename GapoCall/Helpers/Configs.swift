//
//  Contanst.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/5/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import UIKit

enum Configs {
    static let mqtt: String = "staging-mqtt.gapo.vn"
}

class AppConfig: NSObject {
    class func setCacheDictionary(value: [String: Any], key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }

    class func getCacheDictionary(key: String) -> [String: Any]? {
        return UserDefaults.standard.dictionary(forKey: key)
    }
}
