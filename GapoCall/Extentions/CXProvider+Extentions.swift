//
//  CXProvider+Extentions.swift
//  GapoCallKit
//
//  Created by manh.le on 9/30/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import CallKit

extension CXProvider {
    func updateConfiguration(includesCallsInRecents: Bool) {
        if #available(iOS 11.0, *) {
            let newConfig = configuration
            guard newConfig.includesCallsInRecents != includesCallsInRecents else { return }
            newConfig.includesCallsInRecents = includesCallsInRecents
            configuration = newConfig
        }
    }
}
