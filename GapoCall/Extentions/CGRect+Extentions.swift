//
//  CGRect+Extentions.swift
//  GapoCallKit
//
//  Created by manh.le on 10/5/20.
//  Copyright © 2020 manh.le. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    var center: CGPoint { return CGPoint(x: midX, y: midY) }
}
