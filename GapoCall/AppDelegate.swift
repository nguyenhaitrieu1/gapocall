//
//  AppDelegate.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/1/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import UIKit
import GapoMQTT
import Alamofire
import DTMvvm
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var disposeBag: DisposeBag? = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UserInfoManager.shared.user = UserInfo.loadFromCache(key: "cache_user_info")
        connectMQTT()
        GVoipPushManager.shared.voipRegistration()
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func connectMQTT() {
        MQTTManager.shared.connect(host: Configs.mqtt, userId: "\(UserInfoManager.shared.user?.userId ?? 0)", token: UserInfoManager.shared.user?.token ?? "", deviceId: "kajdkfjasfksajflasf")
        
        MQTTManager.shared.rxIncomingCall.subscribe(onNext: { [weak self] (incoming) in
            guard let incoming = incoming else { return }
            self?.handleIncoming(incoming.callId)
        }) => disposeBag
    }
    
    func handleIncoming(_ callId: String?) {
        guard let callId = callId else {
            return
        }
        let vc = CallingVC()
        vc.modalPresentationStyle = .fullScreen
        vc.callService = CallService(inComing: callId, type: .video)
        self.topController()?.present(vc, animated: true, completion: nil)
    }
    
    func topController() -> UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
}

extension AppDelegate {
    
//    func setupFlipper(application: UIApplication) {
//        #if DEBUG
//        let client = FlipperClient.shared()
//        let layoutDescriptorMapper = SKDescriptorMapper(defaults: ())
//        client?.add(FlipperKitLayoutPlugin(rootNode: application, with: layoutDescriptorMapper!))
//        client?.add(FlipperKitNetworkPlugin(networkAdapter: SKIOSNetworkAdapter()))
//        client?.add(FKUserDefaultsPlugin.init(suiteName: nil))
//        client?.start()
//        #endif
//    }
    
}
