//
//  CallEvent.swift
//  GapoMQTT
//
//  Created by Hai Trieu on 10/8/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct CallEventBase: Mappable {
    
    var tag: CallEventTag = .none
    var userId: UInt64 = 0
    var createdAt: Double = 0
    
    public init?(map: Map) { }
    
    mutating public func mapping(map: Map) {
        tag <- map["tag"]
        userId <- map["user_id"]
        createdAt <- map["created_at"]
    }

}

public struct CallEvent<T: Mappable>: Mappable {
    
    var tag: CallEventTag = .none
    var userId: UInt64 = 0
    var data: T?
    
    public init?(map: Map) { }
    
    public mutating func mapping(map: Map) {
        tag <- map["tag"]
        userId <- map["user_id"]
        data <- map["data"]
    }

}
