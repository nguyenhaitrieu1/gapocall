//
//  CallOfferEvent.swift
//  GapoMQTT
//
//  Created by Hai Trieu on 10/8/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct CallOfferEvent: Mappable {

    var target: UInt64 = 0
    var id: String = ""
    
    public init?(map: Map) { }
    
    mutating public func mapping(map: Map) {
        target <- map["target"]
        id <- map["id"]
    }
}
