//
//  CallEventTag.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import Foundation

public enum CallEventTag: Int {
    case none = -1
    case incoming = 0
    case status = 1
    case setup = 2
    case hangup = 3
    case ignore = 4//no answer
    case timeout = 5
    case offer = 6 //create offer
}
