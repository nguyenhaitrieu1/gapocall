//
//  Constant.swift
//  GapoMQTT
//
//  Created by Hai Trieu on 10/1/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

struct Constant {

    static let domain: String = ""
    static let userId: String = ""
    static let deviceId: String = "00000000-0000-0000-0000-000000000120"
    static let token: String = ""
    
}
