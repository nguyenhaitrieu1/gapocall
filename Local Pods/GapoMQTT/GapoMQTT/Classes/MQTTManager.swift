//
//  MQTTManager.swift
//  GapoMQTT
//
//  Created by Hai Trieu on 10/1/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import CocoaMQTT
import ObjectMapper
import RxCocoa
import RxSwift

public typealias CallEventObservable = (type: CallEventTag, jsonString: String)

public class MQTTManager {

    public static var shared = MQTTManager()
    
    private var mqtt: CocoaMQTT?
    
    open var isReconnect = false
    
    private var userId: String = ""
    
    public let rxCallEvent: BehaviorRelay<CallEventObservable> = BehaviorRelay<CallEventObservable>(value: (.none, ""))

    public let rxIncomingCall: BehaviorRelay<CallEventIncoming?> = BehaviorRelay<CallEventIncoming?>(value: nil)
    public let rxOfferEvent: BehaviorRelay<CallOfferEvent?> = BehaviorRelay<CallOfferEvent?>(value: nil)
    
    open func connect(host: String, userId: String, token: String, deviceId: String) {
        let port = 1883
        self.userId = userId
        let password = "Bearer \(token)"
        let clientID = String(format: "ios_\(userId)_\(deviceId)")
        mqtt = CocoaMQTT(clientID: clientID, host: host, port: UInt16(port))
        mqtt?.username = userId
        mqtt?.password = password
        mqtt?.keepAlive = 30
        mqtt?.delegate = self
        mqtt?.cleanSession = true
        mqtt?.autoReconnect = true
        //start connect
        print("connect mqtt")
        _ = mqtt?.connect(timeout: 60)
        
    }
}

extension MQTTManager: CocoaMQTTDelegate {
    
    public func mqttDidPing(_ mqtt: CocoaMQTT) {
        print("mqttDidPing")
    }
    
    public func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
        print("mqttDidReceivePong")
    }
    
    public func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
        print("mqtt mqttDidDisconnect \(String(describing: err?._code))")
        if err != nil {
            switch err?._code {
            case 57: //change connection type
                isReconnect = true
            case 7, 61: //server disconnected, try connect to other host
                isReconnect = true
            case 32: //disconnect in background mode
                isReconnect = true
            default:
                break
            }
        }
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
        print("didConnectAck")
        mqtt.subscribe("call_service/v1/\(userId)/event")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
        print("didPublishMessage")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
        print("didPublishAck")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
        //let topic = message.topic
        guard let messageString = message.string else { return }
        print("mqtt did received message: \(messageString)")
        if let event = CallEventBase(JSONString: messageString) {
            rxCallEvent.accept((event.tag, messageString))
            switch event.tag {
            case .incoming:
                if let offer = CallEvent<CallEventIncoming>(JSONString: messageString) {
                    rxIncomingCall.accept(offer.data)
                }
            case .offer:
                if let offer = CallEvent<CallOfferEvent>(JSONString: messageString) {
                    rxOfferEvent.accept(offer.data)
                }
            default:
                break
            }
        }
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topics: [String]) {
        print("didSubscribeTopic")
    }
    
    public func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
        print("didUnsubscribeTopic")
    }
    
    
}

