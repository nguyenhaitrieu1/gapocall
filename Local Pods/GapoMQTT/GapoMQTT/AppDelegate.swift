//
//  AppDelegate.swift
//  GapoMQTT
//
//  Created by Hai Trieu on 10/1/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("connect mqtt")
        MQTTManager.shared.connect(host: "staging-mqtt.gapo.vn", userId: "1043162664", token: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImp0aSI6IjEwNDMxNjI2NjQuMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAwLjE2MDIxNDE4MTAifQ.eyJpc3MiOiJnYXBvLnZuIiwiYXVkIjoiaW9zLmdhcG8udm4iLCJqdGkiOiIxMDQzMTYyNjY0LjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMC4xNjAyMTQxODEwIiwiaWF0IjoxNjAyMTQxODAwLCJuYmYiOjE2MDIxNDE4MDAsInVpZCI6MTA0MzE2MjY2NCwiZXhwIjoxNjAyMTU2MjAwfQ.cUYyk31dVnZs1Y93583H-gUmx1n98QxT8AYife4ouEK7c-KArrdmLDud4F2XK0zmdmz4vCuTILNoXmNw2r5FL2GC4ahAYmAY9U8t4B5GKvoZdVB1mYNgFTdUZnYIOlnNyDVcS9C0ilZfPEdD5RqkQz4y_quYVzeaYuTsxmyCCyY", deviceId: "00000000-0000-0000-0000-000000000000")
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

