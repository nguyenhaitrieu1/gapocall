//
//  JanusResponseAttach.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusResponseAttach: Mappable {
    
    public init?(map: Map) { }
    
    public var handleId: UInt64 = 0
    
    public mutating func mapping(map: Map) {
        handleId <- map["id"]
    }
    
}
