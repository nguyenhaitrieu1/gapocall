//
//  PluginData.swift
//  GapoCall
//
//  Created by manh.le on 10/9/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public class PluginData: Mappable {
    
    public var result: PluginResult?
    var videocall: String?
    required init() { }
    
    required public init?(map: Map) { }
    
    public func mapping(map: Map) {
        result <- map["data.result"]
        videocall <- map["data.videocall"]
    }
}

public class PluginResult: Mappable {
    
    public var event: PluginEvent = .unknow
    var username: String?
    
    required init() { }
    
    required public init?(map: Map) { }
    
    public func mapping(map: Map) {
        event <- map["event"]
        username <- map["username"]
    }
}

public enum PluginEvent: String, CaseIterable {
    case unknow = ""
    case registered = "registered"
    case incomingcall = "incomingcall"
    case accepted = "accepted"
}
