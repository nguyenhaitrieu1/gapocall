//
//  JanusResponseBase.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public enum JanusResponseStatus: String {
    case success = "success"
    case error = "error"
    case ack = "ack"
    case event = "event"
}


public protocol JanusResponse {
    
    var status: JanusResponseStatus { get set}
    var transactionId: String { get set }
}

extension JanusResponse where Self: Mappable {
    
    public mutating func baseMapping(map: Map) {
        status <- map["janus"]
        transactionId <- map["transaction"]
    }
}




