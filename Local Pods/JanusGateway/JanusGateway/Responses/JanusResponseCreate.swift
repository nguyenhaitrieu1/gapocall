//
//  JanusResponseCreate.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusResponseCreate: Mappable {
    
    public init?(map: Map) { }
    
    public var sessionId: UInt64 = 0
    
    public mutating func mapping(map: Map) {
        sessionId <- map["id"]
    }
    
}
