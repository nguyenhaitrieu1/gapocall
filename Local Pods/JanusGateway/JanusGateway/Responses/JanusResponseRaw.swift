//
//  JanusResponseRaw.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusResponseRaw: JanusResponse, Mappable {
    
    public init?(map: Map) { }
    
    public var status: JanusResponseStatus = .success
    public var transactionId: String = ""
    
    var sessionId: UInt64?
    var sender: UInt64?
    
    public var plugingData: PluginData?
    public var jsep: JanusJsep?
    
    public mutating func mapping(map: Map) {
        baseMapping(map: map)
        sessionId <- map["session_id"]
        sender <- map["sender"]
        plugingData <- map["plugindata"]
        jsep <- map["jsep"]
    }
    
}
