//
//  JanusResponse.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusResponseBase<JanusData: Mappable>: Mappable, JanusResponse {
    
    public init?(map: Map) { }
    
    public var status: JanusResponseStatus = .success
    public var transactionId: String = ""
    
    var sender: UInt64 = 0
    public var data: JanusData?
    public var pluginData: JanusData?
    public var jsep: JanusJsep?
    
    public mutating func mapping(map: Map) {
        baseMapping(map: map)
        data <- map["data"]
        pluginData <- map["plugindata"]
        jsep <- map["jsep"]
    }
    
}
