//
//  JanusResponseRegister.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusResponseRegister: Mappable {
    
    public init?(map: Map) { }
    
    public var plugin: JanusPluginType = .echo
    
    public mutating func mapping(map: Map) {
        plugin <- map["plugin"]
    }
    
}
