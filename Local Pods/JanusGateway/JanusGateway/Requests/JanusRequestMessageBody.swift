//
//  JanusRequestBody.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public enum BodyRequestType: String {
    case register = "register"
    case call = "call"
    case accept = "accept"
    case hangup = "hangup"
}

public class JanusRequestMessageBody: NSObject, Mappable {
    
    override init() {
        super.init()
    }

    public convenience required init?(map: Map) {
        self.init()
    }
    
    public convenience init(userId: String, sessionId: UInt64, token: String? = nil) {
        self.init()
        self.userId = userId
        self.sessionId = "\(sessionId)"
        self.token = token
    }
    
    public var request: BodyRequestType = .register
    
    public var userId: String = ""
    var token: String? = ""
    var sessionId: String = ""
    var video: Bool = true
    var audio: Bool = true
    
    public func mapping(map: Map) {
        request <- map["request"]
        userId <- map["username"]
        audio <- map["audio"]
        video <- map["video"]
        token <- map["token"]
        sessionId <- map["sid"]
    }
    
}
