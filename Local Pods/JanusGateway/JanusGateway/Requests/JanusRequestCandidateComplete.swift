//
//  JanusRequestCandidateComplete.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/12/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusRequestCandidateComplete: Mappable {
    
    public init?(map: Map) { }
    
    var completed: Bool = false
    
    public init(_ completed: Bool = true) {
        self.completed = completed
    }
    
    public mutating func mapping(map: Map) {
        completed <- map["completed"]
    }
    
}
