//
//  JanusRequest.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

class JanusRequest: Mappable {
    
    required init?(map: Map) { }
    
    func mapping(map: Map) { }
    
}
