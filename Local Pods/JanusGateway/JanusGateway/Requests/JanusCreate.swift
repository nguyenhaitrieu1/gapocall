//
//  JanusCreate.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/6/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusCreate: Mappable {
    
    public var janus: String = ""
    public var transaction: String = ""
    
    public init?(map: Map) { }
    
    public init() { }
    
    public mutating func mapping(map: Map) {
        janus <- map["janus"]
        transaction <- map["transaction"]
    }

}
