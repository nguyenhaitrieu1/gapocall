//
//  JanusKeepAlive.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/6/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusKeepAlive: Mappable {
    
    var janus: String = ""
    public var transaction: String = ""
    var sessionId: UInt64 = 0
    
    public init?(map: Map) { }
    
    public init(transaction: String, sessionId: UInt64) {
        self.janus = "keepalive"
        self.sessionId = sessionId
        self.transaction = transaction
    }
    
    public mutating func mapping(map: Map) {
        janus <- map["janus"]
        transaction <- map["transaction"]
        sessionId <- map["session_id"]
    }

}
