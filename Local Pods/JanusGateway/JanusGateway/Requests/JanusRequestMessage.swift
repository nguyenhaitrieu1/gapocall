//
//  JanusRequestRegister.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/7/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper

public struct JanusRequestMessage<CandidateRequest: Mappable>: Mappable {
    
    public var janus: String = ""
    public var transactionId: String = ""
    var handleId: UInt64 = 0
    public var sessionId: UInt64 = 0
    var sender: UInt64 = 0
    public var body: JanusRequestMessageBody?
    public var jsep: JanusJsep?
    public var candidate: CandidateRequest?
    
    public init?(map: Map) { }
    
    public init(transaction: String = Utilites.randomString(length: 12), sessionId: UInt64, handleId: UInt64, body: JanusRequestMessageBody? = nil, jsep: JanusJsep? = nil, candidate: CandidateRequest? = nil) {
        self.janus = "message"
        self.transactionId = transaction
        self.sessionId = sessionId
        self.handleId = handleId
        self.body = body
        self.jsep = jsep
        self.candidate = candidate
    }
    
    public mutating func mapping(map: Map) {
        janus <- map["janus"]
        transactionId <- map["transaction"]
        handleId <- map["handle_id"]
        sessionId <- map["session_id"]
        body <- map["body"]
        jsep <- map["jsep"]
        candidate <- map["candidate"]
    }

}
