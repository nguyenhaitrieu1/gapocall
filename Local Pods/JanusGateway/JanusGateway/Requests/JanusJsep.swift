//
//  JanusRequestJsep.swift
//  GapoCall
//
//  Created by Hai Trieu on 10/8/20.
//  Copyright © 2020 Hai Trieu. All rights reserved.
//

import ObjectMapper
import WebRTC

extension RTCSdpType {
    
    init(str: String) {
        switch str {
        case "offer":
            self = RTCSdpType.offer
        case "pranswer":
            self = RTCSdpType.prAnswer
        case "answer":
            self = RTCSdpType.answer
        default:
            self = RTCSdpType.offer
        }
    }
}

public struct JanusJsep: Mappable {
    
    public init?(map: Map) { }
    
    public init(sdp: RTCSessionDescription) {
        self.type = RTCSessionDescription.string(for: sdp.type)
        self.sdp = sdp.sdp
    }
    
    var type: String = ""
    var sdp: String = ""
    
    public mutating func mapping(map: Map) {
        type <- map["type"]
        sdp <- map["sdp"]
    }

}

extension JanusJsep {
    
    public func toSdp() -> RTCSessionDescription {
        return RTCSessionDescription(type: RTCSdpType(str: type), sdp: sdp)
    }
}
